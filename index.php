// Place this at every nested directory level with Guest access; 
// .htaccess +Index option may not work properly
<?php
// Get the requested path
$requestedPath = $_SERVER['REQUEST_URI'];

// Convert the URL path into a physical file path
$directoryPath = $_SERVER['DOCUMENT_ROOT'] . $requestedPath;

// Check if the directory exists
if (is_dir($directoryPath)) {
    // Open the directory
    if ($handle = opendir($directoryPath)) {
        echo "<ul>";

        // Read all the files and directories within the directory
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != ".." && $entry != "index.php") {
                // Create a relative link to the file/directory
                echo "<li><a href='$requestedPath$entry'>$entry</a></li>";
            }
        }

        echo "</ul>";

        // Close the directory handle
        closedir($handle);
    } else {
        echo "Error: Unable to open directory.";
    }
} else {
    echo "Error: Directory does not exist.";
}
?>